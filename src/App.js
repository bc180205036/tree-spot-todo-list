
import "bootstrap/dist/css/bootstrap.min.css"
import {   Routes, Route,  BrowserRouter } from 'react-router-dom';
import Todolist from "./todolist";
import Home from "./Home";

function App() {
  return (

    <div className="App">
      
    <BrowserRouter> 
    <Routes>
    <Route path="/" element={<Home/>} />
          <Route path="/todolist" element={<Todolist/>} />
          
          </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
