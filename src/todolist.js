import React, { useEffect, useState } from 'react'
import { MdOutlineDeleteForever } from "react-icons/md";

function Todolist() {
    const [list,setList]=useState([]);

    useEffect(() => {
        const fetchData = async () => {
          
            const response = await fetch('https://jsonplaceholder.typicode.com/todos');
            if (!response.ok) {
              setList(null);
            }
    
            const treeSpotData= await response.json();
            setTimeout(() => {
                setList(treeSpotData)
                
              },1000);
              console.log(list); 
                 };
           
   
   
        
        fetchData();
      },[list]);
      const handleDelete = async (postId) => {
        let url="https://jsonplaceholder.typicode.com/todos/";
        let fullUrl=url+postId;
          const response = await fetch(fullUrl, {
            method: 'DELETE',
          });
    
          if (!response.ok) {
            throw new Error('Failed to delete todo');
          }
          setTimeout(() => {
            setList((prevData) => prevData.filter((post) => post.id !== postId));
          }, 2000);
         
        
      };
    
  return (
    <div>
<div className='container pt-5'>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
    
  {list.map((item,index) => (
            <tr key={index}>
              <td>{item.id}</td>
              <td>{item.title}</td>
              <td><button className='btn btn-danger' onClick={() => handleDelete(item.id)}><MdOutlineDeleteForever /></button></td>
            </tr>
          ))}  
  </tbody>
</table>
</div>
    </div>
  )
}

export default Todolist